using System;
using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Banking.Transactions
{
	public class WithdrawTransaction : ITransaction
	{
		private const string TransactionInformation = "Withdraw transaction amount = ";
		public WithdrawTransaction(double amount)
		{
			TransactionTime = DateTime.Now;
			Information = $"{TransactionInformation}{amount} at {TransactionTime}";
		}

		public string Information { get; }
		public DateTime TransactionTime { get; }
	}
}