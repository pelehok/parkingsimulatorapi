using System;
using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Banking.Transactions
{
	public class TransferTransaction : ITransaction
	{
		private const string TransactionInformation = "Transfet transaction amount = ";
		public TransferTransaction(double amount,IAccount accountTo)
		{
			TransactionTime = DateTime.Now;
			Information = $"{TransactionInformation}{amount} to {accountTo.Name} at {TransactionTime.ToString()}";
		}

		public string Information { get; }
		public DateTime TransactionTime { get; }
	}
}