using System;

namespace ParkingSimulator.Core.Banking.Transactions.Abstract
{
	public interface ITransaction
	{
		string Information { get; }
		DateTime TransactionTime { get; }
	}
}