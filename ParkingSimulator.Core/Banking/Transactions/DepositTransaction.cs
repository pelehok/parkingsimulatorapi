using System;
using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Banking.Transactions
{
	public class DepositTransaction : ITransaction
	{
		private const string TransactionInformation = "Deposit transaction amount = ";
		public DepositTransaction(double amount)
		{
			TransactionTime = DateTime.Now;
			Information = $"{TransactionInformation}{amount} at {TransactionTime.ToString()}";
		}

		public string Information { get; }
		public DateTime TransactionTime { get; }
	}
}