using ParkingSimulator.Core.Banking.Transactions;
using ParkingSimulator.Core.Exceptions;
using ParkingSimulator.Core.Exceptions.BankingExceptions;

namespace ParkingSimulator.Core.Banking
{
	public class Account : IAccount
	{
		private double _balance; 
		
		public string Name { get; set; }
		public TransactionLog TransactionLog { get; }

		public double Balance
		{
			get { return _balance; }
		}

		public Account()
		{
			TransactionLog = new TransactionLog();
		}
		
		public void Deposit(double amount)
		{
			if(amount<=0)
			{
				throw new EmptyBalanceException("");
			}

			_balance += amount;
			TransactionLog.AddTransaction(new DepositTransaction(amount));
		}

		public void Withdraw(double amount)
		{
			if (Balance < amount)
			{
				throw new TransactionException("");
			}
			else if(amount<=0)
			{
				throw new EmptyBalanceException("");
			}

			_balance -= amount;
			TransactionLog.AddTransaction(new WithdrawTransaction(amount));
		}

		public void TransferFunds(IAccount destination, double amount)
		{
			Withdraw(amount);
			destination.Deposit(amount);
			TransactionLog.AddTransaction(new TransferTransaction(amount, destination));
		}
	}
}