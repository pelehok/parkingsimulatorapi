using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;
using ParkingSimulator.Core.Banking.Transactions.Abstract;
using ParkingSimulator.Core.Helpers;

namespace ParkingSimulator.Core.Banking
{
	public class TransactionLog
	{
		private List<ITransaction> _transaction { get; set; }
		private Timer SaveTransactionTimer { get; set; }

		public TransactionLog()
		{
			_transaction = new List<ITransaction>();
			SaveTransactionTimer = new Timer(ParkingSimulatorSettings.SaveTransactionSec);
			SaveTransactionTimer.Elapsed += SaveTransactionTimerOnElapsed;
		}

		private void SaveTransactionTimerOnElapsed(object sender, ElapsedEventArgs e)
		{
			var timeNow = DateTime.Now;
			for (int i = 0; i < _transaction.Count; i++)
			{
				if (timeNow.Subtract(_transaction[i].TransactionTime).TotalSeconds >
				    ParkingSimulatorSettings.SaveTransactionSec)
				{
					_transaction.Remove(_transaction[i]);
					FileTransactionHelper.WriteToFile(_transaction[i]);
				}
			}
		}

		public void AddTransaction(ITransaction transaction)
		{
			_transaction.Add(transaction);
		}

		public override string ToString()
		{
			StringBuilder logInformation = new StringBuilder("");
			foreach (var transaction in _transaction)
			{
				logInformation.Append($"{transaction.Information}\r\n");
			}

			return logInformation.ToString();
		}
	}
}