namespace ParkingSimulator.Core.Banking
{
	public interface IAccount
	{
		string Name { get; set; }
		double Balance { get; }
		TransactionLog TransactionLog { get; }
		void Deposit(double amount);
		void Withdraw(double amount);
		void TransferFunds(IAccount destination, double amount);
	}
}