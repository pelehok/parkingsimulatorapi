using System;

namespace ParkingSimulator.Core.Exceptions.BankingExceptions
{
	public class EmptyBalanceException : Exception
	{
		public EmptyBalanceException(string message) : base(message)
		{
		}
	}
}