using System;

namespace ParkingSimulator.Core.Exceptions.ParkingException
{
	public class ParkingFullException : Exception
	{
		public ParkingFullException(string message) : base(message)
		{
		}
	}
}