using System;
using ParkingSimulator.Core.Banking;

namespace ParkingSimulator.Core.Transport.Abstract
{
	public abstract class Vehicle
	{
		public IAccount BankAccount { get; set; }
		public string VehicleName { get; set; }

		protected Vehicle()
		{
			BankAccount = new Account();
		}

		public override string ToString()
		{
			return $"{VehicleName}. Balance {BankAccount.Balance}";
		}
	}
}