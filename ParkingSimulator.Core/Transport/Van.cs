using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.Transport
{
	public class Van : Vehicle
	{
		public override string ToString()
		{
			return $"Van {base.ToString()}";
		}
	}
}