using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.Transport
{
	public class MotorBike : Vehicle
	{
		public override string ToString()
		{
			return $"Motor Bike {base.ToString()}";
		}
	}
}