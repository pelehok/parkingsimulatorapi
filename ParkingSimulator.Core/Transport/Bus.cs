using System;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.Transport
{
	public class Bus : Vehicle
	{
		public override string ToString()
		{
			return $"Bus {base.ToString()}";
		}
	}
}