using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Helpers
{
	public class FileTransactionHelper
	{
		private const string FileName = "Transaction.log.txt";

		public static void WriteToFile(ITransaction transaction)
		{
			var information = $"{transaction.Information}\r\n";
			System.IO.File.AppendAllText(FileName, information);
		}

		public static string ReadTransactionLog()
		{
			return System.IO.File.ReadAllText(FileName);
		}
	}
}