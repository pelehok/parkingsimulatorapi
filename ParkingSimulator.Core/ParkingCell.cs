using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Exceptions.BankingExceptions;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core
{
	public class ParkingCell
	{
		public Vehicle Vehicle { get; set; }
		private double RentalRate { get; set; }

		private bool IsHaveFine { get; set; }
		private double Fine { get; set; }
		
		public ParkingCell(Vehicle vehicle, double rentalRate)
		{
			Vehicle = vehicle;
			RentalRate = rentalRate;
			IsHaveFine = false;
		}

		public bool IsPickUp => IsHaveFine;

		public void RentPayment(IAccount transferTo)
		{
			if (IsHaveFine)
			{
				try
				{
					Vehicle.BankAccount.TransferFunds(transferTo, Fine);
					IsHaveFine = false;
				}
				catch (EmptyBalanceException e)
				{
					Fine += RentalRate * ParkingSimulatorSettings.FineCoefficient + RentalRate;
				}
			}
			else
			{
				try
				{
					Vehicle.BankAccount.TransferFunds(transferTo, RentalRate);
				}
				catch (EmptyBalanceException e)
				{
					IsHaveFine = false;
				}
			}
		}
	}
}