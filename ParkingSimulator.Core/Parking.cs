using System.Collections.Generic;
using System.Linq;
using System.Timers;
using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Exceptions.ParkingException;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core
{
	public class Parking
	{
		private static Parking _instance = null;

		public static Parking GetInstance()
		{
			return _instance ?? (_instance = new Parking());
		}

		private Parking()
		{
			InitProperty();
		}


		private int _capasity = ParkingSimulatorSettings.MaxParkingSize;
		private Timer _timer { get; set; }

		public List<ParkingCell> Cells { get; set; }

		public IAccount BankAccount { get; set; }

		public void AddVehicle(Vehicle vehicle)
		{
			if (Cells.Count + 1 > _capasity)
			{
				throw new ParkingFullException("Parking full. Don't have free spaces");
			}
			else
			{
				var vehiclesRentalRate = 0.0;
				switch (vehicle)
				{
					case Car _:
						vehiclesRentalRate = ParkingSimulatorSettings.RentalRateForCar;
						break;
					case Bus _:
						vehiclesRentalRate = ParkingSimulatorSettings.RentalRateForBus;
						break;
					case Van _:
						vehiclesRentalRate = ParkingSimulatorSettings.RentalRateForVan;
						break;
					case MotorBike _:
						vehiclesRentalRate = ParkingSimulatorSettings.RentalRateForMotorBike;
						break;
				}

				Cells.Add(new ParkingCell(vehicle, vehiclesRentalRate));
			}
		}

		public bool RemoveVehicle(Vehicle vehicle)
		{
			if (Cells.Count == 0)
			{
				return false;
			}
			else
			{
				var vehicleCell = Cells.FirstOrDefault(x => x.Vehicle.Equals(vehicle));
				if (!vehicleCell.IsPickUp)
				{
					return false;
				}

				Cells.Remove(vehicleCell);
				return true;
			}
		}

		private void InitProperty()
		{
			BankAccount = new Account();
			BankAccount.Deposit(ParkingSimulatorSettings.DefaultParkingBalance);
			Cells = new List<ParkingCell>();
			_timer = new Timer(ParkingSimulatorSettings.RentalTimeSec * 1000);
			_timer.Elapsed += Rental;
			_timer.Start();
		}

		private void Rental(object sender, ElapsedEventArgs e)
		{
			foreach (var cell in Cells)
			{
				cell.RentPayment(BankAccount);
			}
		}

		public int NumberFreeCells()
		{
			return ParkingSimulatorSettings.MaxParkingSize - Cells.Count;
		}

		public int NumberBusyCells()
		{
			return Cells.Count;
		}
	}
}