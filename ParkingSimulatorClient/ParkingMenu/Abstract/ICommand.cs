namespace ParkingSimulatorClient.ParkingMenu.Abstract
{
	public interface ICommand
	{
		string Description { get; }
		bool Execute();
	}
}