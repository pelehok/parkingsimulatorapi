using System;
using ParkingSimulatorClient.Services;

namespace ParkingSimulatorClient.ParkingMenu
{
	public class GetNameVehicleMenu 
	{
		public static string PrintMenu()
		{
			var service = new ParkingInfoRequestService();
			
			if (service.GetNumberBusyCells() != 0)
			{
				Console.WriteLine("Which vehicle do you want? Choice number");

				var vehicles = service.GetVehicle();

				for (int i = 0; i < vehicles.Count; i++)
				{
					Console.WriteLine($"{i+1} {vehicles[i].ToString()}");
				}
				
				string userChoice;
				var vehicleIndex = -1;
				do
				{
					userChoice = Console.ReadLine();
				} while (!int.TryParse(userChoice, out vehicleIndex) || vehicleIndex > vehicles.Count ||
				         vehicleIndex <= 0);

				return vehicles[vehicleIndex - 1].VehicleName;

			}
			else
			{
				Console.WriteLine("Parking is empty");
			}

			return null;
		}
	}
}