using ParkingSimulatorClient.ParkingMenu.Abstract;

namespace ParkingSimulatorClient.ParkingMenu.MainMenuCommands
{
	public class ViewTransactionHistory : ICommand
	{
		public string Description { get; } = "Show transaction history.";
		public bool Execute()
		{
			//Console.WriteLine(FileTransactionHelper.ReadTransactionLog());
			return false;
		}
	}
}