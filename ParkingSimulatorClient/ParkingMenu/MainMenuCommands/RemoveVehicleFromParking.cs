using System;
using ParkingSimulatorClient.ParkingMenu.Abstract;
using ParkingSimulatorClient.Services;

namespace ParkingSimulatorClient.ParkingMenu.MainMenuCommands
{
	public class RemoveVehicleFromParking : ICommand
	{
		public string Description { get; } = "Remove vehicle from parking.";
		public bool Execute()
		{
			var vehicleName = GetNameVehicleMenu.PrintMenu();
			if (vehicleName == null) return false;
			
			Console.WriteLine($"Do you want to remove {vehicleName.ToString()} vehicle?({Yes_No[0]}, {Yes_No[1]})");
			if (UserChoiceYes_No())
			{
				ParkingVehicleRequestService service = new ParkingVehicleRequestService();
				service.DeleteVehicle(vehicleName);
			}

			return false;
		}
		
		private readonly string[] Yes_No = new[] {"Y", "N"};

		private bool UserChoiceYes_No()
		{
			string userChoice;
			do
			{
				userChoice = Console.ReadLine();
			} while (!userChoice.ToUpper().Equals(Yes_No[0]) && !userChoice.ToUpper().Equals(Yes_No[1]));

			return userChoice.ToUpper().Equals(Yes_No[0]);
		}
		
	}
}