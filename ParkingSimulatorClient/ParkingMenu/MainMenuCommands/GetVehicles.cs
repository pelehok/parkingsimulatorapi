using System;
using ParkingSimulatorClient.ParkingMenu.Abstract;
using ParkingSimulatorClient.Services;

namespace ParkingSimulatorClient.ParkingMenu.MainMenuCommands
{
	public class GetVehicles : ICommand
	{
		public string Description { get; } = "Get information about all vehicles in parking.";
		public bool Execute()
		{
			var service = new ParkingInfoRequestService();
			var vehicles = service.GetVehicle();
			foreach (var vehicle in vehicles)
			{
				Console.WriteLine(vehicle.ToString());
			}

			return false;
		}
	}
}