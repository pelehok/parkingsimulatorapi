using System;
using ParkingSimulatorClient.ParkingMenu.Abstract;
using ParkingSimulatorClient.Services;

namespace ParkingSimulatorClient.ParkingMenu.MainMenuCommands
{
	public class GetParkingBalance : ICommand
	{
		public string Description { get; } = "Get parking balance.";
		public bool Execute()
		{
			var service = new ParkingInfoRequestService();

			Console.WriteLine($"Parking balance = {service.GetParkingBalance()}");
			return false;
		}
	}
}