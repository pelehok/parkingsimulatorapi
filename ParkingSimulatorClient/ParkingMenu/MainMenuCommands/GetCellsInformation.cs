using System;
using ParkingSimulatorClient.ParkingMenu.Abstract;
using ParkingSimulatorClient.Services;

namespace ParkingSimulatorClient.ParkingMenu.MainMenuCommands
{
	public class GetCellsInformation : ICommand
	{
		public string Description { get; } = "Get number free/busy parking spaces";
		public bool Execute()
		{
			var service = new ParkingInfoRequestService();
			
			Console.WriteLine($"Free {service.GetNumberFreeCells()} parking spaces");
			Console.WriteLine($"Busy {service.GetNumberBusyCells()} parking spaces");

			return false;
		}
	}
}