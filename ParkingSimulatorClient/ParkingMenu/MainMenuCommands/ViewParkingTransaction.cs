using System;
using ParkingSimulatorClient.ParkingMenu.Abstract;
using ParkingSimulatorClient.Services;

namespace ParkingSimulatorClient.ParkingMenu.MainMenuCommands
{
	public class ViewParkingTransaction : ICommand
	{
		public string Description { get; } = "Show parking transaction history for last minute";
		public bool Execute()
		{
			var service = new ParkingInfoRequestService();

			Console.WriteLine(service.GetTransactionLog().ToString());

			return false;
		}
	}
}