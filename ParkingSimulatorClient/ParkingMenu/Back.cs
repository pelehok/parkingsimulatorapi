using ParkingSimulatorClient.ParkingMenu.Abstract;

namespace ParkingSimulator.ParkingMenu
{
	public class Back : ICommand
	{
		public string Description { get; } = "Back";
		public bool Execute()
		{
			return true;
		}
	}
}