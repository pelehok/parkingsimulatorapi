using System;
using ParkingSimulatorClient.Models;
using ParkingSimulatorClient.ParkingMenu.Abstract;
using ParkingSimulatorClient.Services;

namespace ParkingSimulatorClient.ParkingMenu.AddVehicleCommand.Abstract
{
	public abstract class AddVehicle : ICommand
	{
		public abstract string Description { get; }
		public abstract bool Execute();

		protected void CreatVehicleMenu(Vehicle vehicle)
		{
			Console.WriteLine($"Do you want to add money in vehicle balance? Default value = {50}. ({Yes_No[0]} , {Yes_No[1]})");
			if (UserChoiceYes_No())
			{
				Console.WriteLine($"How much?");
				var userBalance = UserBalance();
				vehicle.BankAccount.Balance = userBalance;
			}
			else
			{
				vehicle.BankAccount.Balance = 50;
			}

			var vehicleName = "";
			do
			{
				Console.WriteLine($"Add a name for the new vehicle. It must a vehicle identifier.");
				vehicleName = Console.ReadLine();
				Console.WriteLine($"Do you want to add a vehicle with name {vehicleName}. ({Yes_No[0]} , {Yes_No[1]})");
			} while (!UserChoiceYes_No());

			vehicle.VehicleName = vehicleName;
			try
			{
				ParkingVehicleRequestService service = new ParkingVehicleRequestService();

				service.AddVehicle(vehicle);
				Console.WriteLine($"You add new vehicle to parking:{vehicle.ToString()}");
			}
			catch (Exception e)
			{
				Console.WriteLine("Parking full");
			}
		}
		
		private readonly string[] Yes_No = new[] {"Y", "N"};
		
		private double UserBalance()
		{
			string userChoice;
			var userBalance = 0.0;
			do
			{
				userChoice = Console.ReadLine();
			} while (!double.TryParse(userChoice, out userBalance) || userBalance <= 0);

			return userBalance;
		}

		private bool UserChoiceYes_No()
		{
			string userChoice;
			do
			{
				userChoice = Console.ReadLine();
			} while (!userChoice.ToUpper().Equals(Yes_No[0]) && !userChoice.ToUpper().Equals(Yes_No[1]));

			return userChoice.ToUpper().Equals(Yes_No[0]);
		}
		
	}
}