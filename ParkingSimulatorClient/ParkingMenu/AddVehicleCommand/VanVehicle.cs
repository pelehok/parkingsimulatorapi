using ParkingSimulatorClient.Models.Transport;
using ParkingSimulatorClient.ParkingMenu.AddVehicleCommand.Abstract;

namespace ParkingSimulatorClient.ParkingMenu.AddVehicleCommand
{
	public class VanVehicle : AddVehicle
	{
		public override string Description { get; } = $"Van";

		public override bool Execute()
		{
			var bus = new Van();
			CreatVehicleMenu(bus);
			return false;
		}
	}
}