using ParkingSimulatorClient.Models.Transport;
using ParkingSimulatorClient.ParkingMenu.AddVehicleCommand.Abstract;

namespace ParkingSimulatorClient.ParkingMenu.AddVehicleCommand
{
	public class MotorBikeVehicle : AddVehicle
	{
		public override string Description { get; } = $"Motor bike";

		public override bool Execute()
		{
			var car = new MotorBike();
			CreatVehicleMenu(car);
			return false;
		}
	}
}