using ParkingSimulatorClient.Models.Transport;
using ParkingSimulatorClient.ParkingMenu.AddVehicleCommand.Abstract;

namespace ParkingSimulatorClient.ParkingMenu.AddVehicleCommand
{
	public class BusVehicle : AddVehicle
	{
		public override string Description { get; } = $"Bus";

		public override bool Execute()
		{
			var bus = new Bus();
			CreatVehicleMenu(bus);
			return false;
		}
	}
}