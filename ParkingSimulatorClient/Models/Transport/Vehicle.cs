using Newtonsoft.Json;

namespace ParkingSimulatorClient.Models
{
	public class Vehicle
	{
		[JsonProperty("bankAccount")]
		public Account BankAccount { get; set; }
		
		[JsonProperty("vehicleName")]
		public string VehicleName { get; set; }

		public Vehicle()
		{
			BankAccount = new Account();
		}

		public override string ToString()
		{
			return $"{VehicleName}. Balance {BankAccount.Balance}";
		}
	}
}