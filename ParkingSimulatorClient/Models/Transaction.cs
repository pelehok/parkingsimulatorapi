using System;

namespace ParkingSimulatorClient.Models
{
	public class Transaction
	{
		public string Information { get; }
		public DateTime TransactionTime { get; }
	}
}