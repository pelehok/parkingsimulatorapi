using Newtonsoft.Json;

namespace ParkingSimulatorClient.Models
{
	public class Account
	{
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("balance")]
		public double Balance { get; set; }
	}
}