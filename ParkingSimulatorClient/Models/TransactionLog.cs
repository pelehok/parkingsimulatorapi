using System.Collections.Generic;
using System.Text;

namespace ParkingSimulatorClient.Models
{
	public class TransactionLog
	{
		public List<Transaction> Transaction { get; set; }
		
		public override string ToString()
		{
			StringBuilder logInformation = new StringBuilder("");
			foreach (var transaction in Transaction)
			{
				logInformation.Append($"{transaction.Information}\r\n");
			}

			return logInformation.ToString();
		}
	}
}