using System.Collections.Generic;
using Newtonsoft.Json;
using ParkingSimulatorClient.Models;

namespace ParkingSimulatorClient.Services
{
	public class ParkingInfoRequestService : ParkingRequestService
	{
		public double GetParkingBalance()
		{
			var balance = CreateRequest("parkinginfo", "getBalance");
			return JsonConvert.DeserializeObject<double>(balance.Result);
		}
		
		public double GetNumberFreeCells()
		{
			var count = CreateRequest("parkinginfo", "getNumberFreeCells");
			return JsonConvert.DeserializeObject<double>(count.Result);
		}
		
		public double GetNumberBusyCells()
		{
			var count = CreateRequest("parkinginfo", "getNumberBusyCells");
			return JsonConvert.DeserializeObject<double>(count.Result);
		}
		
		public List<Vehicle> GetVehicle()
		{
			var count = CreateRequest("parkinginfo", "getVehicle");
			return JsonConvert.DeserializeObject<List<Vehicle>>(count.Result);
		}
		
		public TransactionLog GetTransactionLog()
		{
			var count = CreateRequest("parkinginfo", "GetTransactionLog");
			return JsonConvert.DeserializeObject<TransactionLog>(count.Result);
		}
	}
}