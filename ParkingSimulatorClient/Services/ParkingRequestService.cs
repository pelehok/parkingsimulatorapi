using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ParkingSimulatorClient.Services
{
	public class ParkingRequestService
	{
		protected static string local = "https://localhost:5001";

		private HttpClient _client;

		public ParkingRequestService()
		{
			_client = new HttpClient();
		}

		protected async Task<string> CreateRequest(string controller, string action)
		{
			var client = new HttpClient();

			return await client.GetStringAsync($"{local}/{controller}/{action}");
		}
		
		protected async Task<string> CreateRequestToApi(string apiUrl)
		{
			var client = new HttpClient();

			return await client.GetStringAsync($"{local}/api/{apiUrl}");
		}
		
		protected async Task<string> CreateRequestWithParam(string controller, string action,string parameters)
		{
			var client = new HttpClient();

			return await client.GetStringAsync($"{local}/{controller}/{action}/{parameters}");
		}
	}
}