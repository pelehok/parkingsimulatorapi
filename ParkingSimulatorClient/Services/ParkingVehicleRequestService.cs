using System.Web;
using Newtonsoft.Json;
using ParkingSimulatorClient.Models;
using ParkingSimulatorClient.Models.Transport;

namespace ParkingSimulatorClient.Services
{
	public class ParkingVehicleRequestService : ParkingRequestService
	{
		public void AddVehicle(Vehicle vehicle)
		{
			var vehicleTypeName = "";
			switch (vehicle)
			{
				case Bus _:
					vehicleTypeName = "bus";
					break;
				case Van _:
					vehicleTypeName = "van";
					break;
				case Car _:
					vehicleTypeName = "car";
					break;
				case MotorBike _:
					vehicleTypeName = "motorbike";
					break;
			}

			string query = $"?vehicleTypeName={vehicleTypeName}&" +
			               $"startBalance={vehicle.BankAccount.Balance.ToString()}&" +
			               $"vehicleName={vehicle.VehicleName.ToString()}";
			
			CreateRequestWithParam("ParkingVehicle", "addVehicle",query);
		}
		
		public void DeleteVehicle(string vehicleName)
		{
			CreateRequestWithParam("ParkingVehicle", "deleteVehicle",$"?vehicleName={vehicleName}");
		}
	}
}