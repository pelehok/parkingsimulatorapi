﻿using System;
using ParkingSimulator.ParkingMenu;

namespace ParkingSimulatorClient
{
	class Program
	{
		static void Main(string[] args)
		{
			MainMenu mainMenu = new MainMenu();
			mainMenu.PrintMenu();
		}
	}
}