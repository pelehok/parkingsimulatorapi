using Microsoft.AspNetCore.Mvc;
using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Transport;
using ParkingSimulatorApi.Services.Abstract;

namespace ParkingSimulatorApi.Controllers
{
	/*[Route("api/[controller]")]
	[ApiController]*/
	public class ParkingVehicleController : Controller
	{
		private IParkingVehicleService _parkingService;
		public ParkingVehicleController(IParkingVehicleService service) {
			_parkingService = service;
		}
		
		[HttpGet]
		public void AddVehicle(string vehicleTypeName,string vehicleName,double startBalance)
		{
			var balance = new  Account();
			balance.Deposit(startBalance);

			switch (vehicleTypeName)
			{
				case "bus":
					_parkingService.AddVehicle(new Bus()
					{
						BankAccount = balance,
						VehicleName = vehicleName
					});
					break;
				case "car":
					_parkingService.AddVehicle(new Car()
					{
						BankAccount = balance,
						VehicleName = vehicleName
					});
					break;
				case "van":
					_parkingService.AddVehicle(new Van()
					{
						BankAccount = balance,
						VehicleName = vehicleName
					});
					break;
				case "motorbike":
					_parkingService.AddVehicle(new MotorBike()
					{
						BankAccount = balance,
						VehicleName = vehicleName
					});
					break;
			}
		}
		
		[HttpGet]
		public void DeleteVehicle(string vehicleName)
		{
			_parkingService.DeleteVehicle(vehicleName);
		}
	}
}