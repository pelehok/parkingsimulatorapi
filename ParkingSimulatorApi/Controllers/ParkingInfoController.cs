using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ParkingSimulator.Core.Banking;
using ParkingSimulatorApi.Services.Abstract;

namespace ParkingSimulatorApi.Controllers
{
	public class ParkingInfoController : Controller
	{
		private IParkingService _parkingService;
		public ParkingInfoController(IParkingService service) {
			_parkingService = service;
		}

		public ActionResult<double> GetBalance()
		{
			return Ok(_parkingService.GetParkingBalance());
		}

		public ActionResult<int> GetNumberFreeCells()
		{
			return Ok(_parkingService.GetNumberFreeCells());
		}
		
		public ActionResult<int> GetNumberBusyCells()
		{
			return Ok(_parkingService.GetNumberBusyCells());
		}
		
		public ActionResult<IEnumerable<string>> GetVehicle()
		{
			return Ok(_parkingService.GetVehicle());
		}
		
		public ActionResult<TransactionLog> GetTransactionLog()
		{
			return Ok(_parkingService.GetTransactionLog());
		}
	}
}