using System.Collections.Generic;
using System.Transactions;
using ParkingSimulator.Core;
using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.Core.Transport.Abstract;
using ParkingSimulatorApi.Services.Abstract;

namespace ParkingSimulatorApi.Services
{
	public class ParkingInfoService : IParkingService
	{
		private Parking _parking = Parking.GetInstance();
		
		public double GetParkingBalance()
		{
			return _parking.BankAccount.Balance;
		}

		public int GetNumberFreeCells()
		{
			return _parking.NumberFreeCells();
		}

		public int GetNumberBusyCells()
		{
			return _parking.NumberBusyCells();
		}

		public List<Vehicle> GetVehicle()
		{
			var vehicles = new List<Vehicle>();
			foreach (var cell in _parking.Cells)
			{
				vehicles.Add(cell.Vehicle);
			}

			return vehicles;
		}
		
		public TransactionLog GetTransactionLog()
		{
			return _parking.BankAccount.TransactionLog;
		}
	}
}