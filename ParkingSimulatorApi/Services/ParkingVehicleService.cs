using System.Linq;
using ParkingSimulator.Core;
using ParkingSimulator.Core.Transport.Abstract;
using ParkingSimulatorApi.Services.Abstract;

namespace ParkingSimulatorApi.Services
{
	public class ParkingVehicleService : IParkingVehicleService
	{
		private Parking _parking = Parking.GetInstance();
		public void AddVehicle(Vehicle vehicle)
		{
			_parking.AddVehicle(vehicle);
		}

		public void DeleteVehicle(string vehicleName)
		{
			var vehicle = _parking.Cells.FirstOrDefault(x => x.Vehicle.VehicleName.Equals(vehicleName)).Vehicle;
			_parking.RemoveVehicle(vehicle);
		}
	}
}