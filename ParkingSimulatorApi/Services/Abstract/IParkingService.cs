using System.Collections.Generic;
using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulatorApi.Services.Abstract
{
	public interface IParkingService
	{
		double GetParkingBalance();
		int GetNumberFreeCells();
		int GetNumberBusyCells();
		List<Vehicle> GetVehicle();
		TransactionLog GetTransactionLog();
	}
}