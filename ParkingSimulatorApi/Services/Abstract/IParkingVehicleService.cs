using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulatorApi.Services.Abstract
{
	public interface IParkingVehicleService
	{
		void AddVehicle(Vehicle vehicle);
		void DeleteVehicle(string vehicleName);
	}
}